const username = sessionStorage.getItem("username");
const addRoomButton = document.getElementById('create__room_button')
let printedLetters = 0
let textForGame = ''

const createRoom = () => {
  const roomName = prompt("Please input Room Name")
  if (roomName) {
    socket.emit('CREATE_ROOM', roomName);
  }
}


addRoomButton.addEventListener('click', createRoom)

if (!username) {
  window.location.replace("/login");
}

export const socket = io("http://localhost:3002", { query: { username } });

const joinToRoom = (room) => {
  return () => {
    socket.emit('JOIN_ROOM', room)
  }
}

const showError = (errorString) => {
  alert(errorString)
}

const existingUserHandler = (username) => {
  alert(`User ${username} Already Exist`)
  sessionStorage.removeItem('username')
  window.location.replace("/login");
}

const userCreator = (user) => {
  let name = `${user.name}${user.socketId === socket.id ? ' you' : ''}`
  let progress = user.progress
  let isUserReadyClass = user.isReady ? 'ready' : 'not-ready'

  let cardWrapper = document.createElement('div')
  cardWrapper.className = 'connected_user'
  cardWrapper.id = `${name}__card-game`

  let finishStyle = progress == 100 ? ' background-color: rgb(101, 164, 7);' : ''

  cardWrapper.innerHTML =
    `
    <div class="user_info">
      <div class="user_status ${isUserReadyClass}"></div>
      <div class="user_name">${name}</div>
    </div >
    <div class="progress">
      <div class="progress_status" style="width: ${progress}%;${finishStyle}"></div>
    </div>
    </div>
  `
  return (cardWrapper)
}

const leaveRoomHandler = (roomName) => {
  return () => {
    socket.emit('LEAVE_ROOM', roomName)
  }
}

const leaveRoomDone = ({ roomName, roomInfo }) => {
  const rooms = document.getElementById('rooms-page')
  rooms.classList.remove('display-none')
  const game = document.getElementById('game-page')
  game.classList.add('display-none')
}

const leftSideCreator = (room) => {
  let left = document.createElement('div')
  left.className = 'left_side'

  let roomHeader = document.createElement('h3')
  roomHeader.className = 'game__header'
  roomHeader.innerText = `${room.roomName}`

  let backToRoomButton = document.createElement('a')
  backToRoomButton.className = 'back_to_rooms_button'
  backToRoomButton.id = 'back_to_rooms_button'
  backToRoomButton.innerText = `Back To Rooms`
  backToRoomButton.href = '#'
  backToRoomButton.addEventListener('click', leaveRoomHandler(room.roomName))

  let headerWrapper = document.createElement('div')
  headerWrapper.className = 'left_side'
  headerWrapper.append(roomHeader, backToRoomButton)

  let users = document.createElement('div')
  users.className = 'connected_users'
  users.id = 'connected_users'

  for (let item of room.usersList) {
    users.append(userCreator(item))
  }

  left.append(headerWrapper, users)
  return left
}

const rightSideCreator = (room) => {

  const isReady = room.usersList.filter(user => user.socketId === socket.id)[0].isReady

  const readyHandler = () => {
    return () => {
      socket.emit('CHANGE_USER_STATUS')
    }
  }

  let div = document.createElement('div')
  div.className = 'right_side'

  let rightSideBox = document.createElement('div')
  rightSideBox.className = 'right_side_box'
  rightSideBox.id = 'right_side_box'


  let mainGameWindow = document.createElement('p')
  mainGameWindow.className = 'main_game_window'
  mainGameWindow.id = 'main_game_window'


  let currentController = document.createElement('a')
  currentController.className = isReady ? 'not_ready_button' : 'ready_button'
  currentController.id = 'ready_button'
  currentController.innerText = isReady ? 'NotReady' : 'Ready'

  currentController.addEventListener('click', readyHandler())

  if (!room.isReadyToStart) {
    mainGameWindow.append(currentController)
    rightSideBox.append(mainGameWindow)
  }
  div.append(rightSideBox)
  return div
}

const createRoomPage = (room) => {
  let page = document.getElementById('game-page')
  page.innerHTML = ''
  let left = leftSideCreator(room)
  let right = rightSideCreator(room)
  page.append(left, right)
}

const joinRoomDone = (room) => {
  createRoomPage(room)
  const rooms = document.getElementById('rooms-page')
  rooms.classList.add('display-none')
  const game = document.getElementById('game-page')
  game.classList.remove('display-none')
}



const renderRooms = (allRooms) => {
  const roomList = document.getElementById('rooms__cards')
  roomList.innerHTML = ''

  const createAndUpdateRoomLayout = (room) => {
    if (room.isFull || room.isReadyToStart) {
      return
    }

    let div = document.createElement('div')

    let button = document.createElement('a')
    button.className = 'room__join-button'
    button.id = `${room.roomName}_join_button`
    button.innerText = 'Join'
    button.addEventListener('click', joinToRoom(room.roomName))
    div.id = `room_id_${room.roomName}`
    div.classList.add('rooms__card')

    div.innerHTML =
      `<p class="rooms__connected">${room.usersInRoom} user connected</p>
      <h4 class="rooms__name">${room.roomName}</h4>`
    div.append(button)
    roomList.append(div)
  }
  for (const item in allRooms) {
    createAndUpdateRoomLayout(allRooms[item])
  }
}

const startTimer = (time) => {
  const createTimerBeforeGame = () => {
    document.getElementById('back_to_rooms_button').style.display = 'none'
    const blockForTimer = document.getElementById('right_side_box')
    blockForTimer.innerHTML = ''
    const timerBeforeGame = document.createElement('div')
    timerBeforeGame.className = 'timer_before_game'
    timerBeforeGame.id = 'timer_before_game'
    timerBeforeGame.innerHTML = time
    blockForTimer.append(timerBeforeGame)
  }

  createTimerBeforeGame()
}


const eventHandler = event => {
  let currentSign = textForGame[printedLetters]
  if (event.key === currentSign) {
    const highlightedText = textForGame.substring(0, printedLetters + 1)
    const nextLetter = textForGame.substring(printedLetters + 1, printedLetters + 2)
    const noHighlightedText = textForGame.substring(printedLetters + 2, textForGame.length + 1)

    const progress = highlightedText.length / textForGame.length * 100
    socket.emit('UPDATE_PROGRESS', progress)

    document.getElementById('highlightedTextElement').innerText = highlightedText
    document.getElementById('next_text_letter').innerText = nextLetter
    document.getElementById('noHighlightedTextElement').innerText = noHighlightedText
    printedLetters++
  }
}


const startGame = () => {
  let rightSideBox = document.getElementById('right_side_box')
  rightSideBox.innerHTML = ''

  let rightSideTimer = document.createElement('p')
  rightSideTimer.className = 'game_timer'
  rightSideTimer.id = 'game_timer'
  rightSideTimer.innerText = ''

  let highlightedTextElement = document.createElement('span')
  highlightedTextElement.id = 'highlightedTextElement'

  let noHighlightedTextElement = document.createElement('span')
  noHighlightedTextElement.id = 'noHighlightedTextElement'
  noHighlightedTextElement.innerText = textForGame.substring(1, textForGame.length + 1)

  let nextLetter = document.createElement('span')
  nextLetter.id = 'next_text_letter'
  nextLetter.innerText = textForGame[0]

  let gameTextEl = document.createElement('p')
  gameTextEl.className = 'game_text_element'
  gameTextEl.id = 'game_text_element'
  gameTextEl.append(highlightedTextElement, nextLetter, noHighlightedTextElement)

  let mainGameWindow = document.createElement('p')
  mainGameWindow.className = 'main_game_window'
  mainGameWindow.id = 'main_game_window'
  mainGameWindow.append(gameTextEl)


  rightSideBox.append(rightSideTimer, mainGameWindow)
  document.addEventListener('keydown', eventHandler)

}

const getText = async (number) => {
  let url = `http://localhost:3002/game/texts/${number}`;
  const res = await fetch(url);
  const resultObject = await res.json()
  textForGame = resultObject.text
}

const updateProgressBars = (room) => {
  let users = document.getElementById('connected_users')
  users.innerHTML = ''

  for (let item of room.usersList) {
    users.append(userCreator(item))
  }
}

const generateModalWindow = (text) => {
  const modalText = document.getElementById('modal-text')
  const modal = document.getElementById("myModal");
  const closeBtn = document.getElementsByClassName("close")[0];

  modalText.innerHTML = text
  modal.style.display = "block";

  closeBtn.onclick = function () {
    modal.style.display = "none";
  }
  window.onclick = function (event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
}

const updateGameTimer = (time) => {
  let timerElement = document.getElementById('game_timer')
  timerElement.innerText = `${time} second left`
}

const endGame = (users, room) => {
  printedLetters = 0
  let resultString = ''
  for (let i = 0; i < users.length; i++) {
    resultString = resultString.concat(`#${i + 1} ${users[i]}\n`)
  }
  document.removeEventListener('keydown', eventHandler)
  generateModalWindow(resultString)
}


const resetGame = () => {

}

socket.on('REDIRECT_TO_LOGIN', existingUserHandler)
socket.on('ERROR_ROOM_EXIST', showError)
socket.on('RENDER_ROOMS', renderRooms)
socket.on('JOIN_ROOM_DONE', joinRoomDone)
socket.on('LEAVE_ROOM_DONE', leaveRoomDone)
socket.on('START_TIMER_BEFORE_GAME', startTimer)
socket.on('GET_TEXT', getText)
socket.on('START_GAME', startGame)
socket.on('UPDATE_PROGRESS_BARS', updateProgressBars)
socket.on('UPDATE_GAME_TIMER', updateGameTimer)
socket.on('GAME_END', endGame)
socket.on('RESET_GAME', resetGame)

