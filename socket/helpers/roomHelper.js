const getResultsOfGame = (rooms, currentRoomName) => {
  const roomInfo = rooms.get(currentRoomName)
  if (roomInfo) {
    const resultsOfFinishedUsers = roomInfo.usersList
      .filter(user => user.hasOwnProperty('endTime'))
      .sort((a, b) => a.endTime - b.endTime)
      .map(user => user.name)

    const resultsOfUnfinishedUsers = roomInfo.usersList
      .filter(user => !user.hasOwnProperty('endTime'))
      .sort((a, b) => b.progress - a.progress)
      .map(user => user.name)

    return (resultsOfFinishedUsers.concat(resultsOfUnfinishedUsers))
  }
}

const checkForEndGame = (rooms, currentRoomName) => {
  const roomInfo = rooms.get(currentRoomName)
  const isEveryUserEnd = roomInfo.usersList.every(item => item.progress == 100 ? true : false)
  if (isEveryUserEnd) {
    roomInfo.gameActive = false
    return true
  }
  return false
}


export { checkForEndGame, getResultsOfGame }