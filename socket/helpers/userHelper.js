import socket from ".."

const isUserNameExist = (userName, users) => {
  if (users.has(userName)) {
    return true
  }
  else {
    return false
  }
}

export { isUserNameExist }