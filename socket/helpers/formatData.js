const formatString = (string) => {
  string = string.toLowerCase().trim()
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export { formatString }