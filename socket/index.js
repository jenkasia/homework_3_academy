import * as config from "./config";
import { formatString } from "./helpers/formatData"
import { isUserNameExist } from "./helpers/userHelper"
import { checkForEndGame, getResultsOfGame } from "./helpers/roomHelper"

const rooms = new Map()
const users = new Map()

export default io => {

  io.on("connection", socket => {

    let currentRoomName = ''
    const userName = formatString(socket.handshake.query.username);
    if (isUserNameExist(userName, users,)) {
      socket.emit('REDIRECT_TO_LOGIN', userName)
      return
    }
    else {
      socket.emit('RENDER_ROOMS', Object.fromEntries(rooms))
      users.set(userName, socket.id)
    }

    const isRoomExist = (roomName) => {
      if (rooms.has(roomName)) {
        return true
      }
      else {
        return false
      }
    }

    const removeUserFromRoom = (roomName, socketId) => {
      const roomInfo = rooms.get(roomName)
      roomInfo.usersList = roomInfo.usersList.filter(item => item.socketId !== socketId)
    }

    const checkIsAllUsersReady = (roomInfo) => {
      const isReadyAll = roomInfo.usersList.every(item => {
        return item.isReady
      })
      return (isReadyAll)
    }

    const joinRoom = (roomName) => {
      const roomInfo = rooms.get(roomName)
      if (!roomInfo) return
      roomInfo.usersInRoom++
      if (roomInfo.usersInRoom === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        roomInfo.isFull = true
      }
      roomInfo.usersList.push({
        socketId: socket.id,
        name: userName,
        isReady: false,
        progress: 0
      })
      socket.join(roomName, () => {
      });
      socket.emit("JOIN_ROOM_DONE", rooms.get(roomName))
      socket.broadcast
        .to(roomName)
        .emit(
          'JOIN_ROOM_DONE',
          rooms.get(roomName)
        );
      const allRooms = Object.fromEntries(rooms)
      io.emit('RENDER_ROOMS', allRooms)
    }

    const leaveRoom = (roomName) => {
      const roomInfo = rooms.get(roomName)
      if (!roomInfo) {
        return
      }
      roomInfo.usersInRoom -= 1
      socket.emit('LEAVE_ROOM_DONE', { roomName, roomInfo })
      if (roomInfo.usersInRoom < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        roomInfo.isFull = false
      }
      socket.leave(roomName, () => {
        io.to(roomName).emit("UPDATE_PROGRESS_BARS", roomInfo)
      });
      removeUserFromRoom(roomName, socket.id)
      if (roomInfo.usersInRoom <= 0) {
        rooms.delete(roomName)
      }
      else {
        io.to(roomName).emit("UPDATE_PROGRESS_BARS", rooms.get(roomName))
      }

      const isAllUsersReady = checkIsAllUsersReady(roomInfo)
      if (isAllUsersReady && !roomInfo.gameActive) {
        roomInfo.isReadyToStart = true
        startGame(config.SECONDS_TIMER_BEFORE_START_GAME, roomInfo)
      }
      const allRooms = Object.fromEntries(rooms)
      io.emit('RENDER_ROOMS', allRooms)
    }

    const resetGame = (roomInfo) => {
      roomInfo.isReadyToStart = false;
      roomInfo.gameActive = false;
      roomInfo.usersList.forEach(user => {
        user.progress = 0
        delete user.endTime
        user.isReady = false
      });
      io.to(roomInfo.roomName).emit('JOIN_ROOM_DONE', roomInfo)
      io.emit('RENDER_ROOMS', Object.fromEntries(rooms))
    }

    const gameTimer = (roomName) => {
      const roomInfo = rooms.get(roomName)
      let timeLeft = config.SECONDS_FOR_GAME
      io.to(roomName).emit('UPDATE_GAME_TIMER', timeLeft)
      const interval = setInterval(() => {
        if (!roomInfo.gameActive) {
          clearInterval(interval);
          return
        }
        io.to(roomName).emit('UPDATE_GAME_TIMER', timeLeft - 1)
        timeLeft--;
        if (timeLeft <= 0) {
          clearInterval(interval);
          if (roomInfo && roomInfo.gameActive) {
            const gameResults = getResultsOfGame(rooms, roomName)
            io.to(roomName).emit('GAME_END', gameResults)
            resetGame(roomInfo)
          }
        }
      }, 1000);
    }

    const startGame = (seconds, roomInfo) => {
      let textNumber = Math.floor(Math.random() * Math.floor(config.TEXT_COUNT));
      roomInfo.gameActive = true
      roomInfo.secondsBeforeStart = seconds - 1
      io.to(currentRoomName).emit('START_TIMER_BEFORE_GAME', seconds)
      io.to(currentRoomName).emit('GET_TEXT_TO_GAME', textNumber)
      const interval = setInterval(() => {
        io.to(roomInfo.roomName).emit('START_TIMER_BEFORE_GAME', roomInfo.secondsBeforeStart)
        roomInfo.secondsBeforeStart--;
        if (roomInfo.secondsBeforeStart < 0) {
          clearInterval(interval);
          io.to(roomInfo.roomName).emit('START_GAME')
          gameTimer(roomInfo.roomName)
        }
      }, 1000);
    }

    socket.on('UPDATE_PROGRESS', (progress) => {
      const roomInfo = rooms.get(currentRoomName)
      let user = roomInfo.usersList.find(el => el.socketId == socket.id)
      user.progress = progress
      io.to(currentRoomName).emit('UPDATE_PROGRESS_BARS', roomInfo)

      if (progress == 100) {
        user.endTime = Date.now()
        if (checkForEndGame(rooms, currentRoomName)) {
          const gameResults = getResultsOfGame(rooms, currentRoomName)
          io.to(currentRoomName).emit('GAME_END', gameResults)
          resetGame(roomInfo)
        }
      }
    })

    socket.on('JOIN_ROOM', (roomName) => {
      joinRoom(roomName)
      currentRoomName = roomName
    })

    socket.on('LEAVE_ROOM', (roomName) => {
      leaveRoom(roomName)
      currentRoomName = ''
    })

    socket.on('CREATE_ROOM', (roomName) => {
      roomName = formatString(roomName)
      if (isRoomExist(roomName)) {
        socket.emit('ERROR_ROOM_EXIST', `Room with name ${roomName}, already exist`)
        return
      }
      else {
        const usersList = []
        const roomInfo = {
          roomName: roomName,
          usersInRoom: 0,
          isFull: false,
          isReadyToStart: false,
          secondsBeforeStart: 0,
          usersList: usersList,
          gameActive: false
        }
        rooms.set(roomName, roomInfo)
        joinRoom(roomName)
        currentRoomName = roomName
      }
    })

    socket.on('CHANGE_USER_STATUS', () => {
      const roomInfo = rooms.get(currentRoomName)
      let user = roomInfo.usersList.filter(user => user.socketId === socket.id)[0]
      user.isReady = !user.isReady
      let isAllUsersReady = checkIsAllUsersReady(roomInfo)
      io.to(currentRoomName).emit("JOIN_ROOM_DONE", rooms.get(currentRoomName))
      if (isAllUsersReady) {
        roomInfo.isReadyToStart = true
        startGame(config.SECONDS_TIMER_BEFORE_START_GAME, roomInfo)
      }
      else {
        roomInfo.isReadyToStart = false
      }
      io.emit('RENDER_ROOMS', Object.fromEntries(rooms))
    })

    socket.on('disconnect', () => {
      leaveRoom(currentRoomName)
      io.emit('RENDER_ROOMS', Object.fromEntries(rooms))
      users.delete(userName)
    })

  });
};


